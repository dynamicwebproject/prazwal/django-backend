from products.views import CategoryDetail, CategoryList, DirectMessageDetail, DirectMessageList, OrderDetail, OrderList, ProductDetail, ProductImageDetail, ProductImageList, ProductList, ProductOrderDetail, ProductOrderList, StatusChangeDetail, SubCategoryDetail, SubCategoryList
from django.conf.urls import url
from django.urls import path, include
from rest_framework import urlpatterns
from rest_framework.routers import SimpleRouter
from authentication.views import (
    CustomerUpdate, get_current_user, register_admin, delete_admin, get_admin, login_user,
    ChangePasswordView, register_customer, delete_customer,
    get_customer, UserView
)

app_name="api"

urlpatterns = [
    url(r'^user/me/$', get_current_user, name="UserView"),
    url(r'^user/(?P<pk>[0-9]+)/$', UserView.as_view(), name="UserView"),
    url(r'^admin/$', get_admin),
    url(r'^admin/create/', register_admin),
    url(r'^admin/delete/', delete_admin),
    url(r'^customer/$', get_customer),
    url(r'^customer/create', register_customer),
    url(r'^customer/delete', delete_customer),
    path('customer/<int:pk>', CustomerUpdate.as_view()),
    url(r'^auth/login/', login_user),
    url(r'^auth/change-password', ChangePasswordView.as_view(), name="change_password"),
    url(r'^auth/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),

    path('category/', CategoryList.as_view()),
    path('category/<int:pk>', CategoryDetail.as_view()),

    path('subcategory/', SubCategoryList.as_view()),
    path('subcategory/<int:pk>', SubCategoryDetail.as_view()),

    path('product/', ProductList.as_view()),
    path('product/<int:pk>', ProductDetail.as_view()),

    path('directmessage/', DirectMessageList.as_view()),
    path('directmessage/<int:pk>', DirectMessageDetail.as_view()),

    path('productimage/', ProductImageList.as_view()),
    path('productimage/<int:pk>', ProductImageDetail.as_view()),

    path('order/', OrderList.as_view()),
    path('order/<int:pk>', OrderDetail.as_view()),
    
    path('productorder/', ProductOrderList.as_view()),
    path('productorder/<int:pk>', ProductOrderDetail.as_view()),

    path('status/<int:pk>', StatusChangeDetail.as_view()),
]