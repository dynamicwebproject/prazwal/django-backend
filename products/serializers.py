from authentication.models import User

from rest_framework import serializers
from .models import Category, SubCategory, Product, DirectMessage, ProductImage, Order, ProductOrder

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'category_name', 'description')

class SubCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = SubCategory
        fields = ('id', 'subcategory_name', 'category')

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'category', 'sub_category', 'brand', 'price', 'description', 'size', 'color', 'quantity', 'ratings')
        read_only_fields = ('ratings',)

class DirectMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = DirectMessage
        fields = ('id', 'full_name', 'phone', 'email', 'subject', 'detail')

class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ('id', 'product', 'image')

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'customer', 'status')

class ProductOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductOrder
        fields = ('id', 'product', 'order', 'quantity', 'size', 'color')
        
class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'status',)
