from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import serializers, status
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from random import randrange


from .models import Category, SubCategory, Product, DirectMessage, ProductImage, Order, ProductOrder
from authentication.models import User
from .serializers import CategorySerializer, StatusSerializer, SubCategorySerializer, ProductSerializer, DirectMessageSerializer, ProductImageSerializer, OrderSerializer, ProductOrderSerializer
# from authentication.permissions import AssignmentPermission, SubmissionPermission, GradePermission


# Create your views here.
class CategoryList(APIView):
    # permission_classes=[AssignmentPermission,]
    def get(self, request, format=None):
        category = Category.objects.all()
        serializer = CategorySerializer(category, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class CategoryDetail(APIView):
    # permission_classes=[AssignmentPermission,]
    def get_object(self, pk):
        try:
            return Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            return Response({"error":"Category does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        category = self.get_object(pk)
        serializer = CategorySerializer(category)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        category = self.get_object(pk)
        serializer = CategorySerializer(category, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        category = self.get_object(pk)
        category.delete()
        return Response({"message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)

class SubCategoryList(APIView):
    # permission_classes=[AssignmentPermission,]
    def get(self, request, format=None):
        subcategory = SubCategory.objects.all()
        serializer = SubCategorySerializer(subcategory, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = SubCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class SubCategoryDetail(APIView):
    # permission_classes=[AssignmentPermission,]
    def get_object(self, pk):
        try:
            return SubCategory.objects.get(pk=pk)
        except SubCategory.DoesNotExist:
            return Response({"error":"SubCategory does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        subcategory = self.get_object(pk)
        serializer = SubCategorySerializer(subcategory)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        subcategory = self.get_object(pk)
        serializer = SubCategorySerializer(subcategory, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        subcategory = self.get_object(pk)
        subcategory.delete()
        return Response({"message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)

class ProductList(APIView):
    # permission_classes=[AssignmentPermission,]
    def get(self, request, format=None):
        product = Product.objects.all()
        serializer = ProductSerializer(product, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            product = Product.objects.filter(id=serializer.data.get('id')).first()
            product.ratings = randrange(1, 5)
            product.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class ProductDetail(APIView):
    # permission_classes=[AssignmentPermission,]
    def get_object(self, pk):
        try:
            return Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return Response({"error":"Product does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        product = self.get_object(pk)
        serializer = ProductSerializer(product)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        product = self.get_object(pk)
        serializer = ProductSerializer(product, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        product = self.get_object(pk)
        product.delete()
        return Response({"message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)

class DirectMessageList(APIView):
    # permission_classes=[AssignmentPermission,]
    def get(self, request, format=None):
        directmessage = DirectMessage.objects.all()
        serializer = DirectMessageSerializer(directmessage, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = DirectMessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class DirectMessageDetail(APIView):
    # permission_classes=[AssignmentPermission,]
    def get_object(self, pk):
        try:
            return DirectMessage.objects.get(pk=pk)
        except DirectMessage.DoesNotExist:
            return Response({"error":"DirectMessage does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        directmessage = self.get_object(pk)
        serializer = DirectMessageSerializer(directmessage)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        directmessage = self.get_object(pk)
        serializer = DirectMessageSerializer(directmessage, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        directmessage = self.get_object(pk)
        directmessage.delete()
        return Response({"message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)

class ProductImageList(APIView):
    # permission_classes=[AssignmentPermission,]
    def get(self, request, format=None):
        productimage = ProductImage.objects.all()
        serializer = ProductImageSerializer(productimage, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = ProductImageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class ProductImageDetail(APIView):
    # permission_classes=[AssignmentPermission,]
    def get_object(self, pk):
        try:
            return ProductImage.objects.get(pk=pk)
        except ProductImage.DoesNotExist:
            return Response({"error":"ProductImage does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        productimage = self.get_object(pk)
        serializer = ProductImageSerializer(productimage)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        productimage = self.get_object(pk)
        serializer = ProductImageSerializer(productimage, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        productimage = self.get_object(pk)
        productimage.delete()
        return Response({"message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)

class OrderList(APIView):
    # permission_classes=[AssignmentPermission,]
    def get(self, request, format=None):
        order = Order.objects.all()
        serializer = OrderSerializer(order, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class OrderDetail(APIView):
    # permission_classes=[AssignmentPermission,]
    def get_object(self, pk):
        try:
            return Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            return Response({"error":"Order does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        order = self.get_object(pk)
        serializer = OrderSerializer(order)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        order = self.get_object(pk)
        serializer = OrderSerializer(order, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        order = self.get_object(pk)
        order.delete()
        return Response({"message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)

class ProductOrderList(APIView):
    # permission_classes=[AssignmentPermission,]
    def get(self, request, format=None):
        productorder = ProductOrder.objects.all()
        serializer = ProductOrderSerializer(productorder, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = ProductOrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class ProductOrderDetail(APIView):
    # permission_classes=[AssignmentPermission,]
    def get_object(self, pk):
        try:
            return ProductOrder.objects.get(pk=pk)
        except ProductOrder.DoesNotExist:
            return Response({"error":"ProductOrder does not exist"}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, pk, format=None):
        productorder = self.get_object(pk)
        serializer = ProductOrderSerializer(productorder)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        productorder = self.get_object(pk)
        serializer = ProductOrderSerializer(productorder, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        productorder = self.get_object(pk)
        productorder.delete()
        return Response({"message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)

class StatusChangeDetail(APIView):
    def put(self, request, pk, format=None):
        customer = User.objects.get(pk=pk)
        order = Order.objects.get(customer=customer, status=request.data.get('status'))
        serializer = StatusSerializer(order, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
