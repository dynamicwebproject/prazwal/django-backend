from django.db import models
from authentication.models import User
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.

class Category(models.Model):
    category_name = models.CharField(max_length=200)
    description = models.TextField()

class SubCategory(models.Model):
    subcategory_name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="category_sub")

class Product(models.Model):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="product_category")
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, related_name="product_sub_category")
    brand = models.CharField(max_length=150)
    price = models.PositiveIntegerField()
    description = models.TextField()
    size = models.CharField(max_length=500)
    color = models.CharField(max_length=500)
    quantity = models.PositiveIntegerField()
    ratings = models.PositiveIntegerField(
        default=5,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(5),
        ]
    ) # random rating

class DirectMessage(models.Model):
    full_name = models.CharField(max_length=300)
    phone = models.CharField(max_length=20)
    email = models.EmailField()
    subject = models.CharField(max_length=200)
    detail = models.CharField(max_length=2000)

class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_images')
    image = models.ImageField(upload_to='product_images')


class Order(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="customer")
    status = models.CharField(max_length=200) # separate api

class ProductOrder(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="product_order")
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order_product")
    quantity = models.PositiveIntegerField(default=1)
    size = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)

    class Meta:
        unique_together = ('product', 'order')

# class Reviews(models.Model):
#     product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="reviewed_product")
#     customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reviewer")
#     rating = models.PositiveIntegerField(validators=[MinValueValidator(0),MaxValueValidator(5)])
#     description = models.CharField(max_length=2000)