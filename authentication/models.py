from django.db import models
from django.contrib import auth
from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import send_mail, EmailMessage
import os

# Create your models here.
class User(auth.models.AbstractUser):
    pass


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    email_plaintext_message = "<a href='http://localhost:3000/reset-password/{}'>Reset Password</a>".format(reset_password_token.key)
    email = EmailMessage(
        "Password Reset for LMS",
        email_plaintext_message,
        to=[reset_password_token.user.email]
    )
    email.content_subtype = "html"
    email.send()