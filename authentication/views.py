from django.http.response import JsonResponse
from authentication.permissions import AdministratorPermission
from authentication.models import User
from authentication.serializers import (UserSerializer, ChangePasswordSerializer,
                                        UserDeleteSerializer, LoginSerializer)
from django.shortcuts import render
from .models import User
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import generics
from .permissions import AdministratorPermission, CustomerPermission
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework.views import APIView
# Create your views here.'

class UserView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


@swagger_auto_schema(method='post', request_body=UserSerializer)
@api_view(['POST'])
def register_admin(request):
    serialized = UserSerializer(data=request.data)
    if(not User.objects.filter(is_staff=True)):
        if serialized.is_valid():
            user = User.objects.create(
                username = request.data.get('username'),
                email = request.data.get('email'),
                first_name = request.data.get('first_name'),
                last_name = request.data.get('last_name')
            )
            user.is_staff = True
            user.set_password(request.data.get('password'))
            user.save()
            return Response(serialized.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response({'error': 'An administrator already exists'}, status=status.HTTP_400_BAD_REQUEST)

@swagger_auto_schema(method='delete', request_body=UserDeleteSerializer)
@api_view(['DELETE'])
@permission_classes([AdministratorPermission])
def delete_admin(request):
    try:
        user = User.objects.get(username=request.data.get('username'))
        if user.is_staff:
            user.delete()
        else:
            return Response({"error": "Can only delete an admin"})
    except:
        return Response({"error": "User does not exist"})
    else:
        return Response({"success": "The user was deleted"})

@api_view(['GET'])
def get_admin(request):
    admin = User.objects.filter(is_staff=True).values("id", "username", "email", "first_name", "last_name")
    return Response({"data": admin})

@swagger_auto_schema(method='post', request_body=LoginSerializer)
@api_view(['POST'])
def login_user(request):
    try:
        user = User.objects.get(username = request.data.get('username'))
        if user.check_password(request.data.get('password')):
            return Response({"token": get_tokens_for_user(user), "is_staff": user.is_staff}, status=status.HTTP_200_OK)
        else:
            return Response({"error": "Wrong password"}, status=status.HTTP_400_BAD_REQUEST)
    except:
        return Response({"error":"User does not exist"}, status=status.HTTP_400_BAD_REQUEST)


def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'access': str(refresh.access_token),
    }

class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response({"error":serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

@swagger_auto_schema(method='post', request_body=UserSerializer)
@api_view(['POST'])
def register_customer(request):
    serialized = UserSerializer(data = request.data)
    if serialized.is_valid():
        user = User.objects.create(
            username = request.data.get('username'),
            email = request.data.get('email'),
            first_name = request.data.get('first_name'),
            last_name = request.data.get('last_name'),
        )
        user.set_password(request.data.get('password'))
        user.save()
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    else:
        return Response({"error": serialized._errors}, status=status.HTTP_400_BAD_REQUEST)

@swagger_auto_schema(method='delete', request_body=UserDeleteSerializer)
@api_view(['DELETE'])
@permission_classes([AdministratorPermission])
def delete_customer(request):
    try:
        user = User.objects.get(username=request.data.get('username'))
        print(user)
        if not user.is_staff:
            user.delete()
        else:
            return Response({"error":"Can only delete a customer"})
    except:
        return Response({"error": "The user does not exist"})
    else:
        return Response({"success": "The user was deleted"})

@api_view(['GET'])
def get_customer(request):
    customer = User.objects.filter(is_staff=False).values("id", "email", "username", "first_name", "last_name")
    return Response({"data": customer})


class UserView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class CustomerUpdate(APIView):
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response({"error":"User does not exist"}, status=status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(responses={200: UserSerializer(many=True)})
    def put(self, request, pk, format=None):
        user = self.get_object(pk)
        user.first_name = request.data.get('first_name')
        user.last_name = request.data.get('last_name')
        user.is_staff = bool(request.data.get('is_staff'))
        user.save()
        return Response({"success": "Successfully updated"}, status=status.HTTP_200_OK)


@swagger_auto_schema(method='get', request_body=UserSerializer)
@api_view(['GET'])
def get_current_user(request):
    user = User.objects.filter(username=request.user.username).values("id", "username", "email", "first_name", "last_name", "is_staff").first()
    return JsonResponse({"data": user}, status=status.HTTP_200_OK)