from rest_framework import permissions

class AdministratorPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return True


class CustomerPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return True